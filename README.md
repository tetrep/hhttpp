# Haskell HTTP Proxy
An HTTP proxy written in Haskell. This is a smaller part of a bigger project to intercept arbitrary network traffic.

## TODO
- [ ] support upstream proxy
- [ ] support TLS
- [ ] split into pub/sub for ZMQ
- [ ] intitial intercepting API
- [x] have some semblance of functionality
- [x] parse request content-length
- [x] parse responses
