{-# LANGUAGE RecordWildCards, OverloadedStrings #-}
module HHTTPP.Response where

import HHTTPP.Common
import Data.ByteString (ByteString)
import Data.ByteString.Char8 (pack)
import Text.Parsec.ByteString (Parser)
import Text.Parsec
import Prelude hiding (concat)

data Response = Response {
  http_version :: ByteString,
  status_code :: ByteString,
  status_msg :: ByteString,
  response_rest :: CommonBody
} deriving (Show, Read, Eq, Ord)

parse_response :: Parser Response
parse_response =
  parse_msg_version >>= (\version' ->
  consume_spaces >>
  parse_status_code >>= (\status_code' ->
  consume_spaces >>
  parse_status_msg >>= (\status_msg' ->
  consume_eol >>
  parse_headers_and_body >>= (\(headers', body') ->
  return $ Response version' status_code' status_msg' (CommonBody headers' body')))))

parse_status_code :: Parser ByteString
parse_status_code = fmap pack (many (noneOf " \r\n"))

parse_status_msg :: Parser ByteString
parse_status_msg = fmap pack (many (noneOf "\r\n"))

{-
instance Print_http ResponseHead where
  print_http ResponseHead {..} = http_version <> " " <> status_code <> " " <> status_msg

instance Print_http Response where
  print_http Response {..} = print_http prehead <> "\n" <> concat (map print_http headers) <> "\n" <> body
-}
