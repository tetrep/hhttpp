{-# LANGUAGE OverloadedStrings, RankNTypes #-}

module HHTTPP.TLS where

import Network.TLS
import Data.X509.CertificateStore
import Data.Default.Class (def)
import Network.Socket (HostName)
import Network.TLS.Extra.Cipher (ciphersuite_all)
import Pipes
import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import Control.Exception (Exception, catch, handle)

pipe_tls :: (Exception e) => Context -> Context -> (e -> IO ()) -> IO ()
pipe_tls server_ctx client_ctx e = catch (runEffect $ fromContext server_ctx >-> toContext client_ctx) e

sendTLS :: MonadIO m => Context -> ByteString -> m ()
sendTLS ctx = \bs -> sendData ctx (BL.fromChunks [bs])

recvTLS :: MonadIO m => Context -> m (Maybe ByteString)
recvTLS ctx = liftIO $ do
    handle (\Error_EOF -> return Nothing)
             (do bs <- recvData ctx
                 if B.null bs
                    then return Nothing -- I think this never happens
                    else return (Just bs))

fromContext
  :: MonadIO m
  => Context          -- ^Established TLS connection context.
  -> Producer' ByteString m ()
fromContext ctx = loop where
    loop = do
      mbs <- recvTLS ctx
      case mbs of
        Nothing -> return ()
        Just bs -> yield bs >> loop
{-# INLINABLE fromContext #-}

toContext
  :: MonadIO m
  => Context          -- ^Established TLS connection context.
  -> Consumer' ByteString m r
toContext ctx = for cat (\a -> sendTLS ctx a)
{-# INLINABLE toContext #-}

server_params :: Credential -> ServerParams
server_params cred = ServerParams
    {
      serverWantClientCert = False
    , serverCACertificates = []
    , serverDHEParams = Nothing
    , serverShared = default_shared { sharedCredentials = Credentials [cred] }
    , serverHooks = def
    , serverSupported = default_client_supported
    }

default_client_params :: HostName -> ClientParams
default_client_params hostname =
  let shared = default_shared
      c_hooks = default_client_hooks
      c_supported = default_client_supported
  in ClientParams Nothing (hostname, "") {- use SNI -} True Nothing shared c_hooks c_supported

default_shared :: Shared
default_shared =
  let creds = default_creds
      manager = default_manager
      cert_store = default_cert_store
      validation_cache = default_validation_cache
  in Shared creds manager cert_store validation_cache

default_creds :: Credentials
default_creds = Credentials []

-- TODO consider using hs-tls/.../SimpleClient.hs:sessionRef
default_manager :: SessionManager
default_manager = noSessionManager

default_cert_store :: CertificateStore
default_cert_store = makeCertificateStore []

default_validation_cache :: ValidationCache
default_validation_cache = def

default_client_hooks :: ClientHooks
default_client_hooks = def { onServerCertificate = \_ _ _ _ -> return [] }

default_client_supported :: Supported
default_client_supported =
  let versions = [ SSL2, SSL3, TLS10, TLS11, TLS12 ]
      ciphers = ciphersuite_all
      compressions = [nullCompression]
      hash_signatures = [(x, y) | x <- [ HashSHA1, HashSHA224, HashSHA256, HashSHA384, HashSHA512 ],
                                  y <- [ SignatureRSA, SignatureDSS, SignatureECDSA ]]
  in Supported versions ciphers compressions hash_signatures True True True False
