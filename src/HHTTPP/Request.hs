{-# LANGUAGE RecordWildCards, OverloadedStrings #-}
module HHTTPP.Request where

import HHTTPP.Common
import Text.Parsec.ByteString (Parser)
import Text.Parsec
import Data.Monoid ((<>))
import Data.ByteString (ByteString, append, intercalate)
import Data.ByteString.Char8 (pack, cons)

import Prelude hiding (concat)

data Request = Request {
  verb :: ByteString,
  path :: ByteString,
  query_params :: [(ByteString, Maybe ByteString)],
  request_version :: ByteString,
  request_rest :: CommonBody
} deriving (Show, Read, Eq, Ord)

parse_request_head :: Parser (ByteString, ByteString, [(ByteString, Maybe ByteString)], ByteString)
parse_request_head =
  parse_request_verb >>= (\verb' ->
  consume_spaces >>
  parse_request_path >>= (\path' ->
  consume_spaces >>
  option [] parse_request_query_params >>= (\qp ->
  consume_spaces >>
  parse_msg_version >>= (\version ->
  return (verb', path', qp, version)))))

parse_request :: Parser Request
parse_request =
  parse_request_head <* consume_eol >>= (\(verb', path', qp, version) ->
  parse_headers_and_body >>= (\(headers', body') ->
  return $ Request verb' path' qp version (CommonBody headers' body')))

parse_request_verb :: Parser ByteString
parse_request_verb = fmap pack (many1 (noneOf " "))

parse_request_path :: Parser ByteString
parse_request_path = fmap pack (many1 (noneOf " ?"))

parse_request_query_params :: Parser [(ByteString, Maybe ByteString)]
parse_request_query_params = char '?' >> parse_request_parameters

parse_request_parameters :: Parser [(ByteString, Maybe ByteString)]
parse_request_parameters = parse_one_pair >>= (\first -> fmap (first:) ((many1 (oneOf "&;") >> parse_request_parameters) <|> return []))
  where
    parse_one_pair :: Parser (ByteString, Maybe ByteString)
    parse_one_pair = fmap pack (many (noneOf " =&;")) >>= (\key -> option (key, Nothing) (char '=' >> fmap pack (many (noneOf " &;")) >>= (\val -> return (key, Just val))))

query_param_string :: [(ByteString, Maybe ByteString)] -> ByteString
query_param_string [] = ""
query_param_string x = ("?" <>) $ intercalate "&" $ map (\(key, maybe_value) -> append key (maybe "" (cons '=') maybe_value)) x

instance Print_http Request where
  print_http Request {..} = verb <> " " <> path <> query_param_string query_params <> " " <> request_version
                             <> print_http request_rest
