module HHTTPP.Util where

import           Crypto.PubKey.RSA (PublicKey, PrivateKey)

import           Control.Exception (SomeException)
import qualified Control.Concurrent.Map as M (Map)

import           Data.ByteString (ByteString)
import           Data.CaseInsensitive (mk, FoldCase)
import           Data.Function (on)
import           Data.String (IsString(..))
import           Data.Maybe (listToMaybe)

import           Database.LevelDB.Base (DB)

eitherToMaybe :: Either e a -> Maybe a
eitherToMaybe (Left _) = Nothing
eitherToMaybe (Right a) = Just a

exceptionToMaybe :: Either SomeException a -> Maybe a
exceptionToMaybe = eitherToMaybe

maybeToEither :: e -> Maybe a-> Either e a
maybeToEither _ (Just a) = Right a
maybeToEither e Nothing = Left e

maybeRead :: (Read a) => String -> Maybe a
maybeRead = fmap fst . listToMaybe . reads

eitherRead :: (Read a) => e -> String -> Either e a
eitherRead e = maybeToEither e . maybeRead

sshow :: (IsString b, Show a) => a -> b
sshow = fromString . show

ciEq :: (FoldCase s, Eq s) => s -> s -> Bool
ciEq = (==) `on` mk

type KeyPair = (PublicKey, PrivateKey, ByteString) -- ByteString is the CA cert's SubjectKeyId
type DB' = (M.Map String (), DB)