{-# LANGUAGE OverloadedStrings #-}
module HHTTPP.GUI where

import qualified Codec.Compression.Zlib as Deflate
import qualified Codec.Compression.GZip as GZip

import           Control.Monad (join, void)
import qualified Control.Concurrent.Map as M (unsafeToList)
import           Control.Exception (SomeException, evaluate, try)
import           Control.Lens ((&))

import           Data.ByteString (ByteString)
import           Data.ByteString.Lazy (fromStrict, toStrict)
import qualified Data.ByteString.Lazy as LBS (ByteString)
import           Data.ByteString.UTF8 (toString)
import           Data.Default.Class (def)
import           Data.Maybe (fromMaybe)
import           Data.String (fromString)

import qualified Database.LevelDB.Base as LevelDB (get)

import qualified Graphics.UI.Threepenny as UI
import qualified Graphics.UI.Threepenny.Core as UICore
import           Graphics.UI.Threepenny.Core (Config(..), Element, UI, Window, (#+), defaultConfig, grid, liftIO, set)

import           HHTTPP.Common (CommonBody(..), lookupHeader)
import           HHTTPP.Request (Request(..))
import           HHTTPP.Response (Response(..))
import           HHTTPP.Util (DB', eitherToMaybe, eitherRead, maybeToEither, ciEq)

startGUI :: DB' -> IO ()
startGUI db = UICore.startGUI (defaultConfig { jsPort = Just 8023, jsStatic = Just "static" }) (setup db)

isReqKey :: String -> Bool
isReqKey s = take 3 s == "req"

toRespKey :: String -> String
toRespKey s = "resp" ++ drop 3 s

ellipsify :: Int -> String -> String
ellipsify n s = if length s > (n - 3) then take (n - 3) s ++ "..." else s

renderRecord :: (String, Request, Response) -> [UI Element]
renderRecord (k, req, resp) =
  [
    UI.string k
  , UI.string (ellipsify 20 $ toString $ host req)
  , UI.string (ellipsify 10 $ toString $ verb req)
  , UI.string (ellipsify 50 $ toString $ path req)
  , UI.string (ellipsify 10 $ toString $ status_code resp)
  ]

renderError :: ByteString -> [UI Element]
renderError e = [UI.string "EPIC FAIL", UI.string (toString e)]

getRecords :: DB' -> IO [Either ByteString (String, Request, Response)]
getRecords (map', db) = do
    ks <- fmap (map fst) $ M.unsafeToList map'
    let dbKeys = map (\k -> (fromString k, fromString . toRespKey $ k)) $ filter isReqKey ks
    mapM (\(reqKey, respKey) -> do
        let f :: (Read a) => ByteString -> ByteString -> IO (Either ByteString a) 
            f e k = join . fmap (\x -> eitherRead x $ toString x) . maybeToEither e <$> LevelDB.get db def k
        mReq <- f "req-get failed" reqKey
        mResp <- either (const $ Right $ Response "" "FAIL" "" (CommonBody [] "")) Right <$> f "resp-get failed" respKey
        return $ (,,) <$> return (toString reqKey) <*> mReq <*> mResp) dbKeys

setup :: DB' -> Window -> UI ()
setup db w = do
  UI.addStyleSheet w "custom.css"
  _ <- set UI.title "HHTTPP" (return w)
  es <- liftIO (fmap (map (either renderError renderRecord)) $ getRecords db)
  void $ UI.getBody w #+ [grid $ [grid_header] ++ es]
    where
      grid_header = [UI.string "id", UI.string "host", UI.string "verb", UI.string "path", UI.string "status"]


-- lenses
class HasCommonBody a where
  getBody :: a -> CommonBody
  setBody :: CommonBody -> a -> a
instance HasCommonBody Request where
  getBody = request_rest
  setBody c r = r { request_rest = c }
instance HasCommonBody Response where
  getBody = response_rest
  setBody c r = r { response_rest = c }

body' :: (HasCommonBody a) => a -> ByteString
body' = body . getBody

header :: (HasCommonBody a) => ByteString -> a -> ByteString
header h a = fromMaybe "" (lookupHeader h (headers (getBody a)))

host :: Request -> ByteString
host = header "host"

hasHeader :: (HasCommonBody a) => ByteString -> a -> Bool
hasHeader h a = maybe False (const True) (lookupHeader h (headers (getBody a)))

decompress :: HasCommonBody a => a -> IO a
decompress r = if      header "Content-Encoding" r `ciEq` "gzip" ||
                       header "Transfer-Encoding" r `ciEq` "gzip" then
                 gzipDecompress (body' r) & fmap (\b ->
                   setBody ((getBody r) { body = fromMaybe (body' r) b }) r) -- TODO real lenses
               else if header "Content-Encoding" r `ciEq` "deflate" ||
                       header "Transfer-Encoding" r `ciEq` "deflate" then
                 deflateDecompress (body' r) & fmap (\b ->
                   setBody ((getBody r) { body = fromMaybe (body' r) b }) r)
               else
                 return r

gzipDecompress :: ByteString -> IO (Maybe ByteString)
gzipDecompress a = fmap (fmap toStrict) $ fmap eitherToMaybe $ (try :: IO LBS.ByteString -> IO (Either SomeException LBS.ByteString))
                    $ evaluate (GZip.decompress (fromStrict a))

deflateDecompress :: ByteString -> IO (Maybe ByteString)
deflateDecompress a = fmap (fmap toStrict) $ fmap eitherToMaybe $ (try :: IO LBS.ByteString -> IO (Either SomeException LBS.ByteString))
                    $ evaluate (Deflate.decompress (fromStrict a))
