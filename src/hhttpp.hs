{-# LANGUAGE OverloadedStrings, MultiWayIf, ScopedTypeVariables, RankNTypes #-}
import Control.Lens (over, _1, (<&>))
import HHTTPP.Request
import HHTTPP.Response
import HHTTPP.Common
import HHTTPP.GUI (startGUI)
import HHTTPP.Http_from_socket_bytestring

import Data.UUID (UUID)
import Text.Parsec.ByteString (Parser)
import Text.Parsec hiding (try)
import Network (listenOn, PortID(..))
import Network.Socket hiding (send, recv)
import Network.Socket.ByteString (sendAll, recv)
import Control.Concurrent (forkIO)
import Data.Maybe (listToMaybe, fromMaybe)
import Data.ByteString (ByteString, length)
import qualified Data.ByteString as BS (break)
import qualified Data.ByteString.Char8 as B (split)
import Data.ByteString.Char8 (pack)
import Data.ByteString.UTF8 (toString)

import Control.Monad
import Data.Monoid
import Data.Function
import Data.CaseInsensitive (mk)
import HHTTPP.Util
import Control.Exception
import Pipes
import Pipes.Prelude (tee)
import Pipes.Network.TCP hiding (send, recv, connect, accept)
import Network.TLS (Credential, contextNew, handshake)
import HHTTPP.TLS
import System.Random (randomIO)
import Data.X509
import System.Hourglass
import Data.Hourglass
import Crypto.PubKey.RSA
import Crypto.PubKey.RSA.PKCS15 (sign)
import Crypto.Hash.Algorithms (SHA256(..))
import Data.ASN1.OID
import Data.ASN1.Types
import Data.String
import Database.LevelDB.Base (Options(..), open, put)
import Database.LevelDB.Internal (unsafeClose)
import Data.Default.Class (def)
import qualified Control.Concurrent.Map as M
import System.Posix.Signals (installHandler, Handler(CatchOnce), sigINT, sigTERM)

import Prelude hiding (length)

show_parse_error :: (Show a) => String -> a -> String
show_parse_error parser_name e = "parser (" ++ parser_name ++ ") failed wit (" ++ filter (/= '\n') (show e) ++ ")"

parse' :: String -> Parser a -> ByteString -> (Maybe a -> IO b) -> IO b
parse' parser_name p v f =
  parse p "" v & either (\e -> do
    putStrLn $ show_parse_error parser_name e
    f Nothing
  ) (f . Just)

hctac :: (Exception e) => (e -> IO a) -> IO a -> IO a
hctac = flip catch

fromSocket' :: MonadIO m => Socket -> Producer' ByteString m ()
fromSocket' s = fromSocket s 4096

connect200response :: ByteString
connect200response = "HTTP/1.1 200 OK\r\nConnection Established\r\n\r\n"

caCertResponse :: KeyPair -> IO ByteString
caCertResponse keys@(_, priv, _) = do
  cert <- encodeSignedObject . sign' priv <$> mkCACert keys
  return $ "HTTP/1.1 200 OK\r\nContent-Type: application/x-x509-ca-cert\r\nContent-Disposition: attachment; filename=\"hhttpp.der\"\r\nContent-Length: " <> (fromString . show . length $ cert) <> "\r\n\r\n" <> cert

setPort :: PortNumber -> SockAddr -> SockAddr
setPort p (SockAddrInet _ h) = SockAddrInet p h
setPort p (SockAddrInet6 _ f h s) = SockAddrInet6 p f h s
setPort _ _ = error "wtf seriously, getaddrinfo returned a unix socket !?"

connect' :: ByteString -> Integer -> IO (Maybe Socket)
connect' host port = do
  out_socket <- socket AF_INET Stream defaultProtocol
  msockaddr <- fmap (setPort (fromInteger port) . addrAddress) . listToMaybe . filter ((/= AF_INET6) . addrFamily) <$>
    getAddrInfo Nothing (Just . toString $ host) Nothing
  msockaddr & maybe (putStrLn ("getAddrInfo failed for (" ++ toString host ++ ":" ++ show port ++ ")") >> return Nothing) (\sockaddr -> do
    e <- try (connect out_socket sockaddr)
    either (\(_ :: SomeException) -> putStrLn ("connect failed for (" ++ toString host ++ ":" ++ show port ++ ")") >> return Nothing) (const (return (Just out_socket))) e)

exceptionHandler :: ByteString -> SomeException -> IO ()
exceptionHandler path' e = putStrLn ("(" ++ toString path'++ "): " ++ show e)

-- monomorphism restriction prevents eta reduction ?
sign' :: (Eq a, Show a, ASN1Object a) => PrivateKey -> a -> SignedExact a
sign' priv obj = fst . objectToSignedExact (\b -> sign Nothing (Just SHA256) priv b & either (\x -> error $ "sign failed" ++ show x) (\o -> (o, SignatureALG HashSHA256 PubKeyALG_RSA, ()))) $ obj

caName :: IsString a => a
caName = "HHTTPP Root CA"

issuerDN :: DistinguishedName
issuerDN = DistinguishedName $ map (over _1 getObjectID) [
                               (DnCountry, "US")
                             , (DnOrganization, caName)
                             , (DnCommonName, caName)
                             ]

mkCACert :: KeyPair -> IO Certificate
mkCACert (pub, _, caId) = do
  d <- dateCurrent
  return $ Certificate 2 1 (SignatureALG HashSHA256 PubKeyALG_RSA)
                           issuerDN
                           (d, timeAdd d (Hours 24*3650))
                           issuerDN
                           (PubKeyRSA pub)
                           (Extensions $ Just [
                               ext False (ExtSubjectKeyId caId),
                               ext True (ExtBasicConstraints True Nothing),
                               ext True (ExtKeyUsage [KeyUsage_cRLSign, KeyUsage_keyCertSign])
                           ])

mkCert :: PublicKey -> ByteString -> Extensions -> IO Certificate
mkCert pub h es = do
  r <- (`mod` 10 ^ (20 :: Integer)) <$> randomIO
  d <- dateCurrent
  return $ Certificate 2
                       r
                       (SignatureALG HashSHA256 PubKeyALG_RSA)
                       issuerDN
                       (d, timeAdd d (Hours 24*3650))
                       (DistinguishedName [(getObjectID DnCommonName, fromString $ toString h)])
                       (PubKeyRSA pub)
                       es

ext :: Extension a => Bool -> a -> ExtensionRaw
ext b e = ExtensionRaw (extOID e) b (extEncode e)

mkCredential :: KeyPair -> ByteString -> IO Credential
mkCredential (pub, priv, caId) h = do
  r <- fromString <$> replicateM 32 randomIO
  cert <- mkCert pub h $ Extensions $ Just [ ext True (ExtBasicConstraints False Nothing)
                                           , ext False (ExtSubjectKeyId r)
                                           , ext False (ExtAuthorityKeyId caId)
                                           , ext True (ExtKeyUsage [KeyUsage_digitalSignature, KeyUsage_keyEncipherment])
                                           ]
  return $ (\chain -> (chain, PrivKeyRSA priv)) $ CertificateChain $ map (sign' priv) [cert]

myConsumer:: (Show a, MonadIO m) => DB' -> UUID -> String -> Parser a -> Consumer' ByteString m ()
myConsumer (map', db) uuid s p = go 0 ""
  where
    go :: MonadIO m => Int -> ByteString -> Consumer' ByteString m ()
    go i acc = do
      str <- await
      parse p "" (acc <> str) & either
        (\_ -> do
            go i (acc <> str))
        (\r -> do
            let key = s ++ take 9 (show uuid) ++ show i
            liftIO $ M.insert key () map'
            put db def (pack key) (sshow r)
            go (i + 1) "")

handleConnect :: DB' -> KeyPair -> ByteString -> Socket -> IO ()
handleConnect db keys path' in_socket = do
  let (host:lport) = B.split ':' path'
  listToMaybe lport & fmap (maybeRead . toString) & join & fromMaybe 443 & (\port -> do
    uuid <- randomIO
    mout_socket <- connect' host port
    mout_socket & maybe (close in_socket) (\out_socket -> do
      hctac (exceptionHandler path') (do
        sendAll in_socket connect200response
        cred <- mkCredential keys host
        server_ctx <- contextNew in_socket $ server_params cred
        handshake server_ctx
        client_ctx <- contextNew out_socket $ default_client_params (toString host)
        handshake client_ctx
        -- TODO catch exceptions from runEffect
        void $ forkIO (runEffect $ fromContext server_ctx >-> tee (toContext client_ctx) >-> myConsumer db uuid "req" parse_request)
        void $ forkIO (runEffect $ fromContext client_ctx >-> tee (toContext server_ctx) >-> myConsumer db uuid "resp" parse_response))))

handleHttp :: DB' -> ByteString -> ByteString -> ByteString -> Socket -> IO ()
handleHttp db path' first_line rest in_socket =
  parse' "parse_http_uri" parse_http_uri path' (maybe (close in_socket) (\(host, port, _) -> do
    uuid <- randomIO
    mout_socket <- connect' (pack host) port
    mout_socket & maybe (close in_socket)
                        (\out_socket -> do
      sendAll out_socket (first_line <> rest)
      -- TODO catch exceptions from runEffect
      void $ forkIO (runEffect $ fromSocket' in_socket >-> tee (toSocket out_socket) >-> myConsumer db uuid "req" parse_request)
      void $ forkIO (runEffect $ fromSocket' out_socket >-> tee (toSocket in_socket) >-> myConsumer db uuid "resp" parse_response)
      )
  ))

connection_handler :: DB' -> KeyPair -> Socket -> IO ()
connection_handler db keys in_socket = do
  recv in_socket 4096 <&> BS.break (\x -> x == ctw8 '\r' || x == ctw8 '\n') >>= (\(first_line, rest) ->
    parse' "parse_request_head" parse_request_head first_line (maybe (close in_socket) (\(verb', path', _, _) ->
      if | ((==) `on` mk) verb' "CONNECT" -> handleConnect db keys path' in_socket
         | ((==) `on` mk) verb' "GET" && ((==) `on` mk) path' "http://hhttpp/" -> caCertResponse keys >>= sendAll in_socket >> close in_socket
         | otherwise -> handleHttp db path' first_line rest in_socket)))


accept_loop :: DB' -> KeyPair -> Socket -> IO ()
accept_loop db keys listen_socket =
  accept listen_socket >>= (\(in_socket, _) ->
  forkIO (connection_handler db keys in_socket) >>
  accept_loop db keys listen_socket)


interruptHandler :: DB' -> IO ()
interruptHandler (map', db) = do
  unsafeClose db
  l <- M.unsafeToList map'
  writeFile "history-keys" $ unlines (map fst l)

main :: IO ()
main = do
  (map' :: M.Map String ()) <- M.fromList . fromMaybe [] . fmap (map (\k -> (k, ())) . lines) . exceptionToMaybe =<<
                                  try (readFile "history-keys")
  db' <- open "history.leveldb" (def { createIfMissing = True }) -- todo larget writeBufferSize
  let db = (map', db')
  _ <- installHandler sigINT (CatchOnce $ interruptHandler db) Nothing
  _ <- installHandler sigTERM (CatchOnce $ interruptHandler db) Nothing
  keys <- maybe gen return . join . fmap maybeRead . exceptionToMaybe =<< try (readFile "hhttpp")
  sock <- listenOn (PortNumber 8191)
  putStrLn "listening on 8191"
  _ <- forkIO (startGUI db)
  accept_loop db keys sock
    where
      gen = do
        (pub, priv) <- generate 128 0x10001
        caId <- fromString <$> replicateM 32 randomIO
        let keys = (pub, priv, caId)
        writeFile "hhttpp" (show keys)
        return keys
